# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

JAVA_PKG_IUSE="doc source"

inherit java-pkg-2 java-pkg-simple


DESCRIPTION="allows C/C++ code to be written inline with Java source code."
HOMEPAGE="https://libgdx.com/wiki/utils/jnigen"
SRC_URI="https://github.com/libgdx/gdx-jnigen/archive/refs/tags/${P}.tar.gz"

LICENSE="Apache-2.0"
SLOT="0"
KEYWORDS="amd64"

DEPEND="
	>=virtual/jdk-1.8:*
"

RDEPEND="
	>=virtual/jre-1.8:*
"

BDEPEND="
	>=dev-java/javaparser-core-3.15.6:0
"


S="${WORKDIR}/${P}/${PN}"

JAVA_GENTOO_CLASSPATH="javaparser-core"
JAVA_SRC_DIR="src/main/java"
