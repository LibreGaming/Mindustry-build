# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

# Skeleton command:
# java-ebuilder --generate-ebuild --workdir . --pom javaparser/javaparser-core/pom.xml --download-uri https://github.com/javaparser/javaparser/archive/refs/tags/javaparser-parent-3.24.2.tar.gz --slot 0 --keywords "amd64" --ebuild javaparser-core-3.24.2.ebuild

EAPI=7

JAVA_PKG_IUSE="doc source"
MAVEN_ID="com.github.javaparser:javaparser-core:3.24.3"

inherit java-pkg-2 java-pkg-simple

MY_P="javaparser-parent-${PV}"

DESCRIPTION="The core parser functionality. This may be all you need."
HOMEPAGE="https://github.com/javaparser/javaparser-core"
SRC_URI="https://github.com/javaparser/javaparser/archive/refs/tags/${MY_P}.tar.gz"

LICENSE="Apache-2.0 LGPL-3"
SLOT="0"
KEYWORDS="amd64"

DEPEND="
	>=virtual/jdk-1.8:*
"

RDEPEND="
	>=virtual/jre-1.8:*
"

BDEPEND="
	dev-java/javacc:0
"

S="${WORKDIR}/javaparser-${MY_P}/${PN}"

#JAVA_SRC_DIR="javaparser/${PN}/src/main/java"
JAVA_SRC_DIR="src/main/java"

src_prepare() {
	default

	javacc -GRAMMAR_ENCODING=UTF-8 \
	-JDK_VERSION=1.8 \
	-STATIC=false \
	-DEBUG_PARSER=false \
	-DEBUG_LOOKAHEAD=false \
	-DEBUG_TOKEN_MANAGER=false \
	-BUILD_PARSER=true \
	-TOKEN_MANAGER_USES_PARSER=true \
	-OUTPUT_DIRECTORY="${JAVA_SRC_DIR}" \
	"${JAVA_SRC_DIR}/../javacc/java.jj" \
	|| die "Parser.java code generation via javacc failed"

	mv -n "${S}"/src/main/javacc-support/com/github/javaparser/* "${S}"/src/main/java/com/github/javaparser/
}
